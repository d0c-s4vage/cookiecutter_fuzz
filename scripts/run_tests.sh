#!/usr/bin/env bash

# this is intended to be run inside of the docker container

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"

cd /cookiecutter

pip install /pytest-autoexplore

pytest --cov cookiecutter tests "$@"
exit_code=$?

dest_path="/pytest-autoexplore/cookiecutter-gl-fuzz-report.json"
cp /cookiecutter/gl-fuzz-report.json "$dest_path"
chmod a+rw "$dest_path"

exit $exit_code
