FROM python:3.7

COPY . /cookiecutter

RUN pip install \
    -e /cookiecutter \
    -r /cookiecutter/test_requirements.txt \
    attrs==19.3.0 \
    bytecode==0.11.0 \
    coverage==5.0.3 \
    importlib-metadata==1.5.0 \
    more-itertools==8.2.0 \
    packaging==20.3 \
    pluggy==0.13.1 \
    py==1.8.1 \
    pyparsing==2.4.6 \
    six==1.14.0 \
    wcwidth==0.1.8 \
    z3-solver==4.8.7.0 \
    zipp==3.1.0

WORKDIR /cookiecutter
ENTRYPOINT ["/cookiecutter/scripts/run_tests.sh"]
