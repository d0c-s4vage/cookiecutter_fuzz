#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"


if [ $# -lt 1 ] ; then
    echo "USAGE: $THIS_PROG <path/to/pytest-autoexplore> PYTEST ARGS" 
    exit 1
fi

cd "$DIR"/../

pytest_autoexplore_path=$(readlink -f "$1")
shift

docker build -t cookiecutter_test .

cmd=(
    docker run
        --rm
        -it
        -v "$pytest_autoexplore_path":/pytest-autoexplore
        cookiecutter_test
)
"${cmd[@]}" "$@"
